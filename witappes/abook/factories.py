import factory
from . import models
from faker_e164.providers import E164Provider


def gen_fakes(n=3):
    [x.save() for x in ContactFactory.create_batch(n)]


def gen_num():
    ukfaker =  factory.Faker
    ukfaker.add_provider(E164Provider)
    return ukfaker('safe_e164', region_code="US").generate()


class ContactFactory(factory.Factory):
    class Meta:
        model = models.Contact

    first_name = factory.Faker('first_name')
    family_name = factory.Faker('last_name')
    email = factory.Faker('email')
    phone = factory.Sequence(lambda n: "{}".format(gen_num()))

