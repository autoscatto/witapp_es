from django.test import TestCase
from .factories import ContactFactory
from rest_framework.test import RequestsClient
from .serializers import ContactSerializer
from django.contrib.auth.models import User
from requests.auth import HTTPBasicAuth
from .models import Contact


class ContactTest(TestCase):
    def setUp(self):
        self.test_data = ContactFactory.create_batch(10)
        self.admin_pwd = 'testadmin'
        self.admin_name = 'testadmin'
        self.admin = User(username=self.admin_name, email='')
        self.admin.set_password(self.admin_pwd)
        self.admin.is_superuser=True
        self.admin.is_staff=True
        self.admin.save()
        for c in self.test_data:
            c.save()
        self.client = RequestsClient()
        self.client.auth = HTTPBasicAuth(self.admin_name, self.admin_pwd)
        self.client.headers.update({'x-test': 'true'})

    def get_item_by_id(self, id):
        request = self.client.get('http://testserver/abook/contacts/{}'.format(id))
        return request.json()

    def test_api_read(self):
        request = self.client.get('http://testserver/abook/contacts/')
        self.assertEqual(request.status_code, 200)
        data = request.json()
        data_results = sorted(data.get('results', []), key=lambda x: x['first_name'])
        test_results = [ContactSerializer(c).data for c in Contact.objects.order_by('first_name').all()]
        self.assertListEqual(data_results, test_results)

    def test_item_get(self):
        item = Contact.objects.order_by('?').first()
        data = self.get_item_by_id(item.id)
        test_data = ContactSerializer(item).data
        self.assertDictEqual(data, test_data)

    def test_item_post(self):
        item = ContactFactory()
        data = ContactSerializer(item).data
        request = self.client.post('http://testserver/abook/contacts/', json=data)
        response = request.json()
        item_id = response.get('id')
        data = self.get_item_by_id(item_id)
        test_data = ContactSerializer(item).data
        test_data['id'] = item_id
        self.assertDictEqual(data, test_data)

    def test_item_update(self):
        item = ContactFactory()
        item.first_name = 'foo'
        item.family_name = 'bar'
        item.email = 'nope@no.no'
        item.id = 1
        data = ContactSerializer(item).data
        request = self.client.patch('http://testserver/abook/contacts/{}/'.format(1), json=data)
        self.assertEqual(request.status_code, 200)
        test_data = self.get_item_by_id(1)
        self.assertDictEqual(data, test_data)

    def test_item_delete(self):
        item = Contact.objects.order_by('?').first()
        test_data = self.get_item_by_id(item.id)
        self.assertEqual(item.id, test_data['id'])
        request = self.client.delete('http://testserver/abook/contacts/{}'.format(item.id))
        self.assertEqual(request.status_code, 204)
        request = self.client.get('http://testserver/abook/contacts/{}'.format(item.id))
        self.assertEqual(request.status_code, 404)


