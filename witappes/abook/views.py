from django.http import HttpResponse
from .models import Contact
from .serializers import ContactSerializer
from .factories import gen_fakes
from rest_framework import viewsets
from django.shortcuts import redirect


class ContactViewSet(viewsets.ModelViewSet):
    queryset = Contact.objects.all().order_by('-first_name')
    serializer_class = ContactSerializer


def index(request):
    return HttpResponse("Hi Witapp!")


def gen_fake(request):
    gen_fakes()
    return redirect('admin:abook_contact_changelist')
