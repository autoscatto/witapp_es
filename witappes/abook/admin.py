from django.contrib import admin
from .models import Contact
from .factories import ContactFactory


def generate_fake_data(modeladmin, request, queryset):
    [x.save() for x in ContactFactory.create_batch(3)]
    generate_fake_data.short_description = "Generate 3 new contacts"
    generate_fake_data.acts_on_all = True


class ContactAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'family_name', 'email', 'phone']
    ordering = ['first_name']


admin.site.register(Contact, ContactAdmin)
