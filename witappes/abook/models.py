from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class Contact(models.Model):
    first_name = models.CharField(max_length=200)
    family_name = models.CharField(max_length=200)
    email = models.EmailField(blank=True)
    phone = PhoneNumberField(help_text="phone number in E164 format")

    def __str__(self):
        return "[{id}] {first} {family}".format(id=self.id, first=self.first_name, family=self.family_name)
