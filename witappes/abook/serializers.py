from .models import Contact
from rest_framework import serializers


class ContactSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = Contact
        fields = ('first_name', 'family_name', 'email', 'phone', 'id')